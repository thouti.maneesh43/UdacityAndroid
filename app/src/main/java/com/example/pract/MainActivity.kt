package com.example.pract

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.example.pract.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var c:TextView
    lateinit var d:EditText
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var bb=Maneesh("Reddy","Thouti")

        binding=DataBindingUtil.setContentView(this,R.layout.activity_main)
        binding.myName=bb
        Log.e("Maneesh","Maneesh")
        val b=findViewById<Button>(R.id.button)
        d=findViewById(R.id.editText)
        val e=d.text
        b.setOnClickListener {
            c=findViewById(R.id.textView)
            c.text=e
        }
    }
}